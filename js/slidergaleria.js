var sliderGaleria = (function () {
    var sliderGaleria = {};
    var position = 0;
    sliderGaleria.Iniciar = function (id) {
        $("#"+id).find("button.left").on('mousedown',function(){
             moveLeft(id);

        });    
        $("#"+id).find("button.right").on('mousedown',function(){
            moveRight(id);
        })  
    };
    function moveLeft(id) {
        position = $("#"+id).scrollLeft();
        $("#"+id).scrollLeft(position-280)
    }
    function moveRight(id) {
        position = $("#"+id).scrollLeft();
        $("#"+id).scrollLeft(position+280)

    }
    return sliderGaleria;
}());
var modalGaleria = (function () {
    var modalGaleria = {}; 
    var slideIndex = 1;
    var idGaleria;
   
    modalGaleria.openModal= function (id,n) {

        document.getElementById(id).style.display = "block";
        idgaleria = "#"+id;

        currentSlide(n);
        $(idGaleria).find(".prev").on('click',function(){
            plusSlides(-1)

        }); 
        $(idGaleria).find(".next").on('click',function(){
            plusSlides(1)
        }); 
    }
    modalGaleria.closeModal= function (id) {
        document.getElementById(id).style.display = "none";
    }
    
   // showSlides(slideIndex);
    
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }
    
    function showSlides(n) {
        var i;
        var slides = $(idGaleria).find(".mySlides");
       
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        }
        slides[slideIndex-1].style.display = "block";
    }
    return modalGaleria;
}());

