var carrosselBanner = (function () {
    var carrosselBanner = {};
    var Matriz = [];
    var itensListaCarrossel = [];
    var itensBulletCarrossel = [];
    var indiceCarrossel = 1;


    carrosselBanner.Iniciar = function () {
        var itens = $("#carrossel-list").children();

        if (itens.length > 1) {
            MontarArray(itens);
        }
        else {
            $("#carrossel .item-carrossel").addClass("active");
        }
    };

    function MontarArray(item) {
        item.map(function (index) {
            $("#carrossel .bullet-carrossel").append('<li class="item-bullet"><a data-indice="' + index + '"><div class="circle-bullet"></div><a></li>');
        });
        itensListaCarrossel = $("#carrossel .item-carrossel").toArray();
        itensBulletCarrossel = $("#carrossel .bullet-carrossel li.item-bullet").toArray();
        $(itensListaCarrossel[0]).addClass("active");
        $(itensBulletCarrossel[0]).addClass("active");
        carrosselLoop();

        //Click Bullets
        $("#carrossel .bullet-carrossel .item-bullet a").click(function () {
            var indice = $(this).attr("data-indice");
            $("#carrossel .item-carrossel").removeClass("active");  
            $("#carrossel .bullet-carrossel .item-bullet").removeClass("active");
            $(itensListaCarrossel[indice]).addClass("active");
            $(itensBulletCarrossel[indice]).addClass("active");
            indiceCarrossel = indice;
            //carrosselLoop("",indice);
        })
    };
    function carrosselLoop() {
        setTimeout(function () {
            if (itensListaCarrossel.length > 1) {
                $("#carrossel .item-carrossel").removeClass("active");
                $("#carrossel .bullet-carrossel .item-bullet").removeClass("active");
                $(itensListaCarrossel[indiceCarrossel]).addClass("active");
                $(itensBulletCarrossel[indiceCarrossel]).addClass("active");
                if (indiceCarrossel == (itensListaCarrossel.length - 1)) {
                    indiceCarrossel = 0;
                }
                else {
                    indiceCarrossel++;
                }
                carrosselLoop();
            }
        }, 10000)
    }

    return carrosselBanner;
}());
