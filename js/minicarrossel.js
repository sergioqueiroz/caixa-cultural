var carrossel = (function () {
    var carrossel = {};
    var itensListaCarrossel = [];
    var itensBulletCarrossel = [];
    var indiceCarrossel = 1;
    var idCarrossel;


    carrossel.Iniciar = function (id) {
        idCarrossel = "#"+id;
        var itens = $(idCarrossel).children();

        if (itens.length > 1) {
            MontarArrayCarrossel(itens);
        }
        else {
            $(idCarrossel+" .item-carrossel").addClass("active");
        }
    };

    function MontarArrayCarrossel(item) {
        item.map(function (index) {
            $(idCarrossel+" .bullet-carrossel").append('<li class="item-bullet"><a data-indice="' + index + '"><div class="circle-bullet"></div><a></li>');
        });
        itensListaCarrossel = $(idCarrossel+" .item-carrossel").toArray();
        itensBulletCarrossel = $(idCarrossel+" .bullet-carrossel li.item-bullet").toArray();
        $(itensListaCarrossel[0]).addClass("active");
        $(itensBulletCarrossel[0]).addClass("active");
        carrosselLoop();

        //Click Bullets
        $(idCarrossel+" .bullet-carrossel .item-bullet a").click(function () {
            var indice = $(this).attr("data-indice");
            $(idCarrossel+" .item-carrossel").removeClass("active");
            $(idCarrossel+" .bullet-carrossel .item-bullet").removeClass("active");
            $(itensListaCarrossel[indice]).addClass("active");
            $(itensBulletCarrossel[indice]).addClass("active");
            indiceCarrossel = indice;
            //carrosselLoop("",indice);
        })
    };
    function carrosselLoop() {
        setTimeout(function () {
            if (itensListaCarrossel.length > 1) {
                $(idCarrossel+" .item-carrossel").removeClass("active");
                $(idCarrossel+" .bullet-carrossel .item-bullet").removeClass("active");
                $(itensListaCarrossel[indiceCarrossel]).addClass("active");
                $(itensBulletCarrossel[indiceCarrossel]).addClass("active");
                if (indiceCarrossel == (itensListaCarrossel.length - 1)) {
                    indiceCarrossel = 0;
                }
                else {
                    indiceCarrossel++;
                }
                carrosselLoop();
            }
        }, 10000)
    }

    return carrossel;
}());
